# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"
SRC_URI:append = " \
    file://0001-twister-remove-python3-tabulate-dependence.patch \
    file://0002-twister-set-toolchain-to-ZEPHYR_GCC_VARIANT.patch \
"

ZEPHYR_INHERIT_CLASSES += "zephyr cmake"
inherit ${ZEPHYR_INHERIT_CLASSES}

require zephyr-kernel-test.inc
require zephyr-sample.inc

ZEPHYR_SRC_DIR = "${S}/tests/kernel/pending/"
OECMAKE_SOURCEPATH = "${ZEPHYR_SRC_DIR}"

DEPENDS += "\
    python3-pyserial-native \
    python3-psutil-native \
    python3-anytree-native \
    python3-ply-native \
"

# Base directory for twister to recursively search for test cases.
# Build all available zephyr tests by default.
# Source https://github.com/zephyrproject-rtos/zephyr/tree/main/tests
ZEPHYRTESTS = "\
    application_development \
    arch \
    benchmarks \
    bluetooth \
    boards \
    boot \
    cmake \
    crypto \
    drivers \
    kernel \
    lib \
    misc \
    net \
    posix \
    subsys \
    unit \
    ztest \
"
# Specify tags of tests that does not build.
ZEPHYRTESTS_EXCLUDE_TAGS = ""
ZEPHYRTESTS_EXCLUDE_TAGS:96b-nitrogen = "fpu"
# Specify tests that should be deleted before image building using twister.
# This can be used when test tags not well defined to exclude specific tests.
# Relative path of 'tests/' at the base of zephyr tree should be used.
ZEPHYRTESTS_DELETE = ""
ZEPHYRTESTS_DELETE:96b-nitrogen = "\
    kernel/profiling \
    kernel/fatal \
    kernel/timer/starve \
    drivers/sensor \
    misc/test_build \
    lib/sprintf \
    subsys/portability \
    crypto/mbedtls \
    benchmarks/footprints \
    benchmarks/latency_measure \
    benchmarks/sched \
    benchmarks/app_kernel \
    benchmarks/sys_kernel \
"
# Deploy the needed files that can be executed and run on DUT using
# twister --test-only.
ARTIFACTS = "\
    twister.json \
    zephyr.hex \
    zephyr.bin \
    zephyr.elf \
"
ARTIFACTS:append:qemu-x86 = "\
    CMakeCache.txt \
    Makefile \
"
ARTIFACTS:append:qemu-cortex-m3 = "\
    CMakeCache.txt \
    Makefile \
"

do_compile() {
    if test -n "${ZEPHYRTESTS}"; then
        tc_root_options="${@' '.join(['-T tests/' + i + ' ' for i in d.getVar('ZEPHYRTESTS').split()])}"
        bbnote "Test case root options: ${tc_root_options}"
        bbnote "Excluded test tags: ${ZEPHYRTESTS_EXCLUDE_TAGS}"
        exclude_tags="${@' '.join(['--exclude-tag ' + i + ' ' for i in d.getVar('ZEPHYRTESTS_EXCLUDE_TAGS').split()])}"
        cd ${S}
        for test in ${ZEPHYRTESTS_DELETE}; do
            rm -rf tests/${test}
            bbnote "Deleted ${test} defined in ZEPHYRTESTS_DELETE ..."
        done
        rm -rf twister-out
        bbnote "Generating twister-out ..."
        # Twister runs both 'cmake' and 'cmake --build' itself to configure and
        # compile test images respectively. All the flags using in cmake.bbclass
        # should be passed to twister so that tests are configured properly.
        # References:
        # Standard target filesystem paths: http://cgit.openembedded.org/openembedded-core/tree/meta/conf/bitbake.conf
        # cmake.bbclass: http://cgit.openembedded.org/openembedded-core/tree/meta/classes/cmake.bbclass
        ./scripts/twister \
            -x=CMAKE_INSTALL_PREFIX:PATH=${prefix} \
            -x=CMAKE_INSTALL_BINDIR:PATH=${@os.path.relpath(d.getVar('bindir'), d.getVar('prefix') + '/')} \
            -x=CMAKE_INSTALL_SBINDIR:PATH=${@os.path.relpath(d.getVar('sbindir'), d.getVar('prefix') + '/')} \
            -x=CMAKE_INSTALL_LIBEXECDIR:PATH=${@os.path.relpath(d.getVar('libexecdir'), d.getVar('prefix') + '/')} \
            -x=CMAKE_INSTALL_SYSCONFDIR:PATH=${sysconfdir} \
            -x=CMAKE_INSTALL_SHAREDSTATEDIR:PATH=${@os.path.relpath(d.getVar('sharedstatedir'), d.getVar('prefix') + '/')} \
            -x=CMAKE_INSTALL_LOCALSTATEDIR:PATH=${localstatedir} \
            -x=CMAKE_INSTALL_LIBDIR:PATH=${@os.path.relpath(d.getVar('libdir'), d.getVar('prefix') + '/')} \
            -x=CMAKE_INSTALL_INCLUDEDIR:PATH=${@os.path.relpath(d.getVar('includedir'), d.getVar('prefix') + '/')} \
            -x=CMAKE_INSTALL_DATAROOTDIR:PATH=${@os.path.relpath(d.getVar('datadir'), d.getVar('prefix') + '/')} \
            -x=PYTHON_EXECUTABLE:PATH=${PYTHON} \
            -x=Python_EXECUTABLE:PATH=${PYTHON} \
            -x=Python3_EXECUTABLE:PATH=${PYTHON} \
            -x=LIB_SUFFIX=${@d.getVar('baselib').replace('lib', '')} \
            -x=CMAKE_INSTALL_SO_NO_EXE=0 \
            -x=CMAKE_TOOLCHAIN_FILE=${WORKDIR}/toolchain.cmake \
            -x=CMAKE_NO_SYSTEM_FROM_IMPORTED=1 \
            -x=CMAKE_EXPORT_NO_PACKAGE_REGISTRY=ON \
            -x=FETCHCONTENT_FULLY_DISCONNECTED=ON \
            -x=ZEPHYR_BASE=${S} \
            -x=ZEPHYR_GCC_VARIANT=${ZEPHYR_GCC_VARIANT} \
            -x=BOARD=${BOARD} \
            -x=ARCH=${ARCH} \
            -x=CROSS_COMPILE=${CROSS_COMPILE} \
            -x=ZEPHYR_SYSROOT=${ZEPHYR_SYSROOT} \
            -x=ZEPHYR_TOOLCHAIN_VARIANT=${ZEPHYR_GCC_VARIANT} \
            -x=EXTRA_CPPFLAGS=${CPPFLAGS} \
            -x=ZEPHYR_MODULES=${ZEPHYR_MODULES} \
            --force-toolchain \
            -p ${@d.getVar('MACHINE').replace("-", "_")} \
            --build-only \
            ${tc_root_options} \
            ${exclude_tags} \
            -vvv
    else
        bbnote "Do nothing as ZEPHYRTESTS is empty."
    fi
}

do_deploy () {
    if test -n "${ZEPHYRTESTS}"; then
        rm -rf "${B}/twister-out"
        # Twister '--test-only' option needs the files defined in
        # ARTIFACTS variable for on device testing. Directory structure
        # must be kept as it is.
        (
            cd "${S}"
            for artifact in ${ARTIFACTS}; do
                find twister-out/ \( -type f -o -type l \) \
                    -name "${artifact}" \
                    -exec cp --verbose --archive --parents {} "${B}" \;
            done
        )
        image_path="${B}/twister-out-${MACHINE}.tar.bz2"
        tar -jcvf ${image_path} -C "${B}" twister-out
        install -D ${image_path} ${DEPLOYDIR}/
    else
        bbnote "Do nothing as ZEPHYRTESTS is empty."
    fi
}
