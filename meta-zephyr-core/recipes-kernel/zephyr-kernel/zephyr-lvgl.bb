include zephyr-sample.inc

ZEPHYR_SRC_DIR = "${S}/samples/subsys/display/lvgl"

python __anonymous() {
    pver = d.getVar("PREFERRED_VERSION_zephyr-kernel")
    shield = None

    if pver == "3.1.0":
        shield = " -DSHIELD=adafruit_2_8_tft_touch_v2_nano"
    else:
        shield = " -DSHIELD=adafruit_2_8_tft_touch_v2"

    d.setVar("EXTRA_OECMAKE:append", shield)
}

COMPATIBLE_MACHINE = "(nrf52840dk-nrf52840|arduino-nano-33-ble)"
